# 自助点餐微信小程序

#### 介绍
自助点餐微信小程序，包含小程序端、后端、后台管理端，支持外卖、到店两种方式，可以拿来做毕设或课设

#### 软件架构
系统总共分为三个端：后端，后台管理系统、微信小程序。
基于当前流行技术组合的前后端分离商城系统： SpringBoot2+MybatisPlus+SpringSecurity+jwt+redis+Vue的前后端分离的商城系统， 包含分类、sku、积分、多门店等

#### 技术栈
Spring Boot，Spring Security oauth2，MyBatis，MyBatisPlus，Redis，lombok，hutoolVue3，Element UI，uniapp(vue3)

#### 部分截图
![输入图片说明](img/image.png)
![输入图片说明](img/image2.png)
![输入图片说明](img/image4.png)
![输入图片说明](img/image7.png)
![输入图片说明](img/image8.png)
![输入图片说明](img/image1.png)
![输入图片说明](img/image6.png)
![输入图片说明](img/image3.png)
![输入图片说明](img/image5.png)

### 最后
如果你的毕设完成遇到了困难，可以联系我，也可以接定制。
有偿提供源码 + 部署，介意勿扰
vx：xtb365
![输入图片说明](img/image9.png)